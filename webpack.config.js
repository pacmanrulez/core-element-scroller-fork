/*
 *     Date: 2023
 *  Package: core-element-scroller
 *   Author: dbf, pacmanrulez, wize-wiz, wizdom
 *   Source: https://gitlab.com/pacmanrulez/core-element-scroller
 *  License: WTFPL v2 (http://www.wtfpl.net/txt/copying/)
 *
 *
 */

const path = require('path');

module.exports = {
  mode: 'production',
  entry: './src/core-element-scroller.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'core-element-scroller.js',
    library: "coreElementScroller",
    libraryTarget: 'umd',
    globalObject: 'this'
  }
};
