/*
 *     Date: 2023
 *  Package: core-element-scroller
 *   Author: dbf, pacmanrulez, wize-wiz, wizdom
 *   Source: https://gitlab.com/pacmanrulez/core-element-scroller
 *  License: WTFPL v2 (http://www.wtfpl.net/txt/copying/)
 *
 *
 */

module.exports = (function () {

    if(!document) {
        throw new Error('Should run a in an environment where DOMDocument is available.');
    }

    const
        PACKAGE = {
            name: 'Scroller'
        },
        WINDOW = window,
        DOCUMENT = document,

        // dependencies
        DOM = require('core-utilities/utility/dom'),
        Package = require('core-utilities/utility/package')
    ;

    // init smoothscroll.
    Smoothscroll = require('smoothscroll-polyfill');
    Smoothscroll.polyfill();

    Scroller = function() {};

    const _scrollTo = function(elements, options, callback) {
        const FROM = elements[1],
              FIXED_OFFSET = options.top.toFixed();
        let timer;
        const onScroll = function () {
            if(timer) {
                clearTimeout(timer);
            }
            timer = setTimeout(function() {
                if ((FROM[FROM === WINDOW ? 'scrollY' : 'scrollTop']).toFixed() === FIXED_OFFSET) {
                    FROM.removeEventListener('scroll', onScroll);
                    if(typeof callback === 'function') {
                        callback();
                    }
                }
            }, 50);
        }

        FROM.addEventListener('scroll', onScroll);
        onScroll();
        FROM.scrollTo(options)
    }

    Scroller.scrollTo = function(elements, options, callback) {

        if(!Array.isArray(elements)) {
            elements = [elements, WINDOW];
        }

        let target = elements[0], from = elements[1];

        options = {
            ...{
                elementOffset: undefined,
                topOffset: 0
            }, ...options
        }

        target = DOM.get(target);
        from = DOM.get(from);

        if((target !== WINDOW && DOM.isElement(target) === false) ||
            (from !== WINDOW && DOM.isElement(from) === false)) {

            return false;
        }

        let top = target.getBoundingClientRect().top + (from === WINDOW ? WINDOW.scrollY : from.scrollTop);

        if(options.elementOffset) {
            top += DOM.getOffset(options.elementOffset);
        }
        if(options.topOffset !== 0) {
            top += options.topOffset;
        }
        if(typeof WINDOW.getComputedStyle === 'function') {
            let scroll_margin_top = parseFloat(getComputedStyle(target).scrollMarginTop);
            top -= scroll_margin_top;
        }

        _scrollTo(elements,{
            top: top,
            behaviour: 'smooth'
        }, callback);

        return top;
    }

    /**
     * Top scroller
     * @param options
     */
    Scroller.scrollTop = function(options, callback) {
        callback = callback || function () {};

        options = {
            ...{
               top: 0,
               behaviour: 'smooth',
               steps: []
            },
            ...options
        }

        return _scrollTo([WINDOW, WINDOW], options, callback);
    }

    Scroller.bind = {
        topScroller: function(elements) {
            if(!Array.isArray(elements)) {
                elements = [elements];
            }
            if(Array.isArray(elements)) {
                for(let index in elements) {
                    let element = elements[index];
                    element = DOM.get(element);

                    let scrolling = false;
                    element.addEventListener('click', (event) => {
                        if(!scrolling) {
                            scrolling = true;
                        }
                        else {
                            return;
                        }

                        event.stopImmediatePropagation();
                        event.stopPropagation();
                        event.preventDefault();

                        Scroller.scrollTop({}, () => {
                            scrolling = false;
                        });
                    });
                }
                return true;
            }
            return false;
        }
    }

    PACKAGE.Scroller = Scroller;
    // assign to global object core
    return Package.return(PACKAGE);
})();